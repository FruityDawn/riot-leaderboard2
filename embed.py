import discord
import re

def pad_string(string, n = 20):
    string_ = str(string)

    while (len(string_) < n):
        string_ += ' '

    return string_[:n - 1]

def remove_emoji(string):
    return re.sub('<.*?>', '', string).strip()

def generate_embed_table(ctx, col_names, cols, title = 'Leaderboard', colour = discord.Colour.yellow()):
    embed = discord.Embed(title = title, colour = colour)

    for i, col_name in enumerate(col_names):
        # embed.add_field(name = col_name, value = '\n'.join([f'{col[i]}᲼᲼᲼᲼' for col in cols][:20]), inline = True)
        embed.add_field(name = col_name, value = '\n'.join([f'{col[i]}⠀⠀⠀⠀' for col in cols][:20]), inline = True)

    return embed

def generate_mobile_table(ctx, col_names, cols, title = 'Leaderboard', colour = discord.Colour.yellow(), padding = 18):
    header = ''.join([pad_string(col_name, padding) for col_name in col_names])
    bar = '=' * len(header)

    rows = [''.join([pad_string(remove_emoji(value).split('#')[0], padding) for value in col]) for col in cols]
    rows = '\n'.join(rows[:20])

    embed_description = f'```\n{header}\n{bar}\n{rows}```'
    embed = discord.Embed(title = title, description = embed_description, colour = colour)

    return embed

