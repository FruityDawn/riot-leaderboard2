from dotenv import load_dotenv
load_dotenv()

from riotwatcher import LolWatcher, TftWatcher, RiotWatcher, ApiError

import discord
from discord.ext import commands, tasks

import asyncio

from util import tag_to_puuid, save_ids, load_ids
from tft import query_tft_user
from lol import get_latest_patch, get_champion_list, query_lol_user
from embed import generate_embed_table, generate_mobile_table

import json
import os
import math
import random

TOKEN = os.getenv('BOT_TOKEN')
TFT_KEY = os.getenv('TFT_KEY')
LOL_KEY = os.getenv('LOL_KEY')

PATCH = os.getenv('PATCH', '14.7.1')

RIOT_REGION = os.getenv('RIOT_REGION')
SERVER_REGION = os.getenv('SERVER_REGION')

POLLING_RATE = int(os.getenv('POLLING_RATE'))

tft_file_name = 'tft.json'
lol_file_name = 'lol.json'

tft_puuids = []
lol_puuids = []

lol_cache = {}
flex_cache = {}
# arena_cache = {}

tft_cache = {}
doubleup_cache = {}

champion_list = []


# puuid's obtained from a watcher can only be used with that api key
rwt = RiotWatcher(TFT_KEY)
tw = TftWatcher(TFT_KEY)

rwl = RiotWatcher(LOL_KEY)
lw = LolWatcher(LOL_KEY)

intents = discord.Intents.default()
intents.message_content = True

bot = commands.Bot(command_prefix = '!', description = 'smarter', intents = intents)

@bot.event
async def on_ready():
    print(f'Logged in as {bot.user}')
    global champion_list

    latest_patch = get_latest_patch()
    print(f'Found latest patch version: {latest_patch}')
    champion_list = get_champion_list(latest_patch)
    print(f'Synced champion list for patch {latest_patch}: {champion_list}')
    check_ranks.start()

@bot.command(name= 'sync', description = 'sync')
@commands.has_permissions(administrator = True)
async def sync(ctx):
    await bot.tree.sync()
    await ctx.send('Command tree synced')

@bot.hybrid_command()
@commands.has_permissions(administrator = True)
async def add(ctx, user):
    tft_puuid = tag_to_puuid(user, rwt, RIOT_REGION)

    if tft_puuid and tft_puuid not in tft_puuids:
        tft_puuids.append(tft_puuid)
        save_ids(tft_file_name, tft_puuids)
        await ctx.send('Successfully added user to TFT', ephemeral = True)
    else:
        await ctx.send('User does not exist or is currently being tracked', ephemeral = True)

    lol_puuid = tag_to_puuid(user, rwl, RIOT_REGION)

    if lol_puuid and lol_puuid not in lol_puuids:
        lol_puuids.append(lol_puuid)
        save_ids(lol_file_name, lol_puuids)

@bot.hybrid_command()
@commands.has_permissions(administrator = True)
async def delete(ctx, user):
    tft_puuid = tag_to_puuid(user, rwt, RIOT_REGION)

    if tft_puuid and tft_puuid in tft_puuids:
        tft_puuids.remove(tft_puuid)
        save_ids(tft_file_name, tft_puuids)
        await ctx.send('Successfully deleted user from TFT', ephemeral = True)
    else:
        await ctx.send('User does not exist or is not currently being tracked', ephemeral = True)


    lol_puuid = tag_to_puuid(user, rwl, RIOT_REGION)

    if lol_puuid and lol_puuid in lol_puuids:
        lol_puuids.remove(lol_puuid)
        save_ids(lol_file_name, lol_puuids)

# League
@bot.hybrid_command(name = 'lol', description = 'Blame Troy')
async def lol(ctx):
    await ctx.send(embed = generate_embed_table(ctx, ['Summoner', 'Rank', 'Win Rate'], lol_cache.values(), 'Solo Queue Leaderboard'))

@bot.hybrid_command(name = 'flex', description = 'Blame Troy')
async def flex(ctx):
    await ctx.send(embed = generate_embed_table(ctx, ['Summoner', 'Rank', 'Win Rate'], flex_cache.values(), 'Flex Leaderboard', colour = discord.Colour.purple()))

@bot.hybrid_command(name = 'lolmobile', description = 'Why')
async def lolmobile(ctx):
    await ctx.send(embed = generate_mobile_table(ctx, ['Summoner', 'Rank', 'Win Rate'], lol_cache.values(), 'Solo Queue Leaderboard', colour = discord.Colour.yellow()))

@bot.hybrid_command(name = 'flexmobile', description = 'Why')
async def flexmobile(ctx):
    await ctx.send(embed = generate_mobile_table(ctx, ['Summoner', 'Rank', 'Win Rate'], flex_cache.values(), 'Flex Leaderboard', colour = discord.Colour.purple()))

# TFT
@bot.hybrid_command(name = 'tft', description = 'Display TFT leaderboard')
async def tft(ctx):
    await ctx.send(embed = generate_embed_table(ctx, ['Summoner', 'Rank', 'Top 4 Rate'], tft_cache.values(), 'TFT Leaderboard'))

@bot.hybrid_command(name = 'doubleup', description = 'Display TFT Double Up leaderboard')
async def du(ctx):
    await ctx.send(embed = generate_embed_table(ctx, ['Summoner', 'Rank', 'Top 4 Rate'], doubleup_cache.values(), 'Double Up Leaderboard', colour = discord.Colour.purple()))

@bot.hybrid_command(name = 'tftmobile', description = 'DUDUDUNGA')
async def tftmobile(ctx):
    await ctx.send(embed = generate_mobile_table(ctx, ['Summoner', 'Rank', 'Top 4 Rate'], tft_cache.values(), 'TFT Leaderboard', colour = discord.Colour.yellow()))

@bot.hybrid_command(name = 'doubleupmobile', description = 'DUDUDUNGA')
async def doubleupmobile(ctx):
    await ctx.send(embed = generate_mobile_table(ctx, ['Summoner', 'Rank', 'Top 4 Rate'], tft_cache.values(), 'Double Up Leaderboard', colour = discord.Colour.purple()))

# Aram
@bot.hybrid_command(name = 'aram', description = 'Generate aram champions')
async def aram(ctx, leader1: discord.Member, num: int = 15, leader2: discord.Member = None):
    if num <= 0:
        await ctx.send('Number of champions must be positive and nonzero')
        return

    if num * 2 > len(champion_list):
        await ctx.send('There are not enough champions for your request!')
        return

    if leader2 is None:
        if leader1 == ctx.author:
            await ctx.send('Other team leader cannot be yourself!')
            return
        else:
            leader2 = ctx.author

    if leader1.bot or leader2.bot:
        await ctx.send('Team leaders cannot be bots!')
        return

    randomised_list = random.sample(champion_list, num * 2)

    team_1 = randomised_list[:num]
    team_2 = randomised_list[num:]

    try:
        await leader1.send(' '.join(team_1))
        await leader2.send(' '.join(team_2))
        await ctx.send(f'Sent {num} champions to team leaders DMs')
    except Exception as e:
        await ctx.send('Error sending DMs (Does someone have them closed?)')

@bot.command(name = 'teamgen', description = 'Generate aram teams')
async def teamgen(ctx, num_teams : int = 2, *players):
    if num_teams > len(players):
        await ctx.send('Not enough players for desired team size')
        return

    shuffled_players = list(players)
    random.shuffle(shuffled_players)

    team_size = math.ceil(len(players) / num_teams)

    # Hack to get around embed display being row rather than column based
    for _ in range(len(players), num_teams * team_size):
        shuffled_players.append('')

    # Chunk into teams of size team_size
    teams = [shuffled_players[i : i + team_size] for i in range(0, len(shuffled_players), team_size)]

    team_headings = [f'Team {team_idx + 1}' for team_idx in range(len(teams))]
    # Flatten teams into rows of embed table
    team_rows = [[teams[team_idx][team_mem_idx] for team_idx in range(len(teams))] for team_mem_idx in range(team_size)]

    await ctx.send(embed = generate_embed_table(ctx, team_headings, team_rows, 'Generated Teams'))


async def poll_tft(puuids):
    print('polling tft')
    new_tft_cache = {}
    new_doubleup_cache = {}

    for puuid in puuids:
        await asyncio.sleep(2)
        user_info = query_tft_user(puuid, rwt, tw, RIOT_REGION, SERVER_REGION)

        if 'RANKED_TFT' in user_info:
            score, info = user_info['RANKED_TFT']
            new_tft_cache[f'{score}{info[2]}{info[0]}'] = info

        if 'RANKED_TFT_DOUBLE_UP' in user_info:
            score, info = user_info['RANKED_TFT_DOUBLE_UP']
            new_doubleup_cache[f'{score}{info[2]}{info[0]}'] = info

    global tft_cache
    global doubleup_cache
    tft_cache = dict(sorted(new_tft_cache.items(), reverse = True))
    doubleup_cache = dict(sorted(new_doubleup_cache.items(), reverse = True))
    print('polling finished')


async def poll_lol(puuids):
    print('polling lol')
    new_lol_cache = {}
    new_flex_cache = {}

    for puuid in puuids:
        await asyncio.sleep(2)
        user_info = query_lol_user(puuid, rwl, lw, RIOT_REGION, SERVER_REGION)

        if 'RANKED_SOLO_5x5' in user_info:
            score, info = user_info['RANKED_SOLO_5x5']
            new_lol_cache[f'{score}{info[2]}{info[0]}'] = info

        if 'RANKED_FLEX_SR' in user_info:
            score, info = user_info['RANKED_FLEX_SR']
            new_flex_cache[f'{score}{info[2]}{info[0]}'] = info

    global lol_cache 
    global flex_cache 
    lol_cache = dict(sorted(new_lol_cache.items(), reverse = True))
    flex_cache = dict(sorted(new_flex_cache.items(), reverse = True))
    print('polling finished')

@tasks.loop(minutes = POLLING_RATE)
async def check_ranks():
    await poll_tft(tft_puuids)
    await poll_lol(lol_puuids)


tft_puuids = load_ids(tft_file_name, default = tft_puuids)
lol_puuids = load_ids(lol_file_name, default = lol_puuids)

bot.run(TOKEN)

