import math
import requests

from util import puuid_to_tag, tag_to_puuid, rank_num, tier_num, get_tier_emoji

def get_latest_patch():
    return requests.get('https://ddragon.leagueoflegends.com/api/versions.json').json()[0]

def get_champion_list(patch):
    return [champion['name'] for champion in requests.get(f'https://ddragon.leagueoflegends.com/cdn/{patch}/data/en_US/champion.json').json()['data'].values()]

def puuid_to_lol(puuid, riot_watcher, lol_watcher, riot_region, server_region):
    name = puuid_to_tag(puuid, riot_watcher, riot_region)
    summoner_id = lol_watcher.summoner.by_puuid(server_region, puuid)['id']

    return name, lol_watcher.league.by_summoner(server_region, summoner_id)

def score_lol(tier, rank, lp):
    return 10000 * tier_num(tier) + 1000 * rank_num(rank) + lp

def score_lol_user(data, queue_type = 'RANKED_SOLO_5x5', emoji = True):
    for queue_info in data:
        if queue_info['queueType'] == queue_type:
            tier = queue_info['tier']
            rank = queue_info['rank']
            lp = queue_info['leaguePoints']

            wins = queue_info['wins']
            losses = queue_info['losses']
            wr = math.floor(100 * wins / (wins + losses))

            score = score_lol(tier, rank, lp)

            tier_emoji = get_tier_emoji(tier) if emoji else ''
            rank_string = f'{tier_emoji}{tier.capitalize()} {rank} {lp}LP'.strip()
            wr_string = f'{wr:.0f}% ({wins}/{wins + losses})'

            return score, rank_string, wr_string

    return None, None, None

def query_lol_user(puuid, riot_watcher, lol_watcher, riot_region, server_region):
    name, summoner_data = puuid_to_lol(puuid, riot_watcher, lol_watcher, riot_region, server_region)

    rtn = {}

    for queue_type in ['RANKED_SOLO_5x5', 'RANKED_FLEX_SR']:
        score, rank_string, wr_string = score_lol_user(summoner_data, queue_type)

        if score is not None:
            rtn[queue_type] = (score, [name, rank_string, wr_string])

    return rtn

