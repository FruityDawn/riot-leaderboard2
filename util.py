import os
import json

def puuid_to_tag(puuid, riot_watcher, riot_region):
    user = riot_watcher.account.by_puuid(riot_region, puuid)
    game_name = user['gameName']
    tag = user['tagLine']
    return f'{game_name}#{tag}'

def tag_to_puuid(tag, riot_watcher, riot_region):
    try:
        name, tagline = tag.split('#')

        return riot_watcher.account.by_riot_id(region = riot_region, game_name = name, tag_line = tagline)['puuid']
    except:
        return None

def tier_num(tier):
	if tier == 'IRON':
		return 1
	if tier == 'BRONZE':
		return 2
	if tier == 'SILVER':
		return 3
	if tier == 'GOLD':
		return 4
	if tier == 'PLATINUM':
		return 5
	if tier == 'EMERALD':
		return 6
	if tier == 'DIAMOND':
		return 7
	if tier == 'MASTER':
		return 8
	if tier == 'GRANDMASTER':
		return 9
	if tier == 'CHALLENGER':
		return 10

	return 0

def rank_num(rank):
	if rank == 'I':
		return 4
	if rank == 'II':
		return 3
	if rank == 'III':
		return 2
	if rank == 'IV':
		return 1

	return 0

def get_tier_emoji(tier):
    if os.getenv('EMOJIS', 'False').lower() == 'true' and os.getenv(tier) is not None:
        return os.getenv(tier) + ' '

    return ''

def save_ids(file_name, data):
    with open(file_name, 'w') as f:
        json.dump(data, f)

def load_ids(file_name, default = []):
    if not os.path.isfile(file_name):
        save_ids(file_name, default)

    with open(file_name, 'r') as f:
        try:
            cache = json.load(f)
        except ValueError:
            cache = []

    return cache

